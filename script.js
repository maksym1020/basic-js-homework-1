/* 
Теоретичні питання
 1. Як можна оголосити змінну у Javascript?

    var (застаріла функція, краще не використовувати)
    let (для змінних, чиї значення можуть змінюватись в майбутньому)
    const (для змінних зі значенням, яке не буде змінюватись після оголошення)

 2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?

    Рядок можна створити:

    - за допомогою лапок - '', "", ``

    const = "some text";

    - явно через функцію конвертування - String

    const someName = String (34);

    - неявно через кастинг типів, коли змінна іншого типу перетворюєтся в string в результаті обробки коду

 3. Як перевірити тип даних змінної в JavaScript?

    за допомогою консолі та команди console.log(typeof );

 4. Поясніть чому '1' + 1 = 11.

    Тому що '1' - string, а 1 - number. Тобто виконується не розрахунок додавання,
    а конкатенація двох значень з конвертуванням всього виразу в string.
*/

// Практичні завдання
// Оголосіть змінну і присвойте в неї число. Перевірте, чи ця змінна має тип "number" і виведіть результат в консоль.

const someNumber = 5;
console.log(typeof someNumber);

// Оголосіть змінну name і присвойте їй ваше ім'я. Оголосіть змінну lastName і присвойте в неї Ваше прізвище.
// Виведіть повідомлення у консоль у форматі `Мене звати ${ім'я}, ${прізвище}` використовуючи ці змінні.

const name = "Maksym";
const lastName = "Skomorokhov";
console.log(`My name is ${name} ${lastName}`);

// Оголосіть змінну з числовим значенням і виведіть в консоль її значення всередині рядка.

const someNumber2 = 5;
console.log(`Some array with ${someNumber2} number`);
// or
console.log("Some array with " + someNumber2 + " number")
